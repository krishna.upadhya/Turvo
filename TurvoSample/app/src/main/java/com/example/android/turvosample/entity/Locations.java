package com.example.android.turvosample.entity;

/**
 * Created by Krishna Upadhya on 6/16/2017.
 */

public class Locations extends BaseDataModel{
    private String title;

    private String image_url;

    private String snippet;

    private double longitude;

    private double latitude;

    public String getTitle ()
    {
        return title;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public String getSnippet()
    {
        return snippet;
    }

    public double getLongitude ()
    {
        return longitude;
    }

    public double getLatitude ()
    {
        return latitude;
    }
}
