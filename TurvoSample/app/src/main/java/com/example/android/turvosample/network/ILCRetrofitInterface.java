package com.example.android.turvosample.network;

import com.example.android.turvosample.entity.LocationData;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface ILCRetrofitInterface {
    @GET()
    Observable<ArrayList<LocationData>> getLocationData(@Url String url);
}
