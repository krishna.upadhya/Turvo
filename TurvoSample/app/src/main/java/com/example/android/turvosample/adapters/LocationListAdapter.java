package com.example.android.turvosample.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.turvosample.R;
import com.example.android.turvosample.entity.Locations;
import com.example.android.turvosample.utils.ImageUtils;
import com.example.android.turvosample.utils.StringUtility;
import com.example.android.turvosample.utils.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Krishna on 4/24/2017.
 */


public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;
    boolean isSwitchView = true;
    private int mGridUnit;
    ArrayList<Locations> mLocationDataList;
    Context mContext;
    public OnLocationItemClicked mLocationItemClickedListener;

    public LocationListAdapter(Context context, ArrayList<Locations> locationList, OnLocationItemClicked locationClickListener) {
        mLocationDataList = locationList;
        mContext = context;
        mGridUnit = Utility.getScreenGridUnitBy32(context);
        mLocationItemClickedListener = locationClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == LIST_ITEM) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_linear_item, null);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_location_item, null);
        }
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Locations item = mLocationDataList.get(position);
        if (item == null) return;
        String imageUrl = item.getImage_url();

        if (getItemViewType(position) == LIST_ITEM) {
            holder.mLocationImage.getLayoutParams().height = mGridUnit * 8;
            holder.mLocationImage.getLayoutParams().width = mGridUnit * 11;
            if (position % 2 == 0) {
                holder.mRootLyt.setBackgroundColor(mContext.getResources().getColor(R.color.article_read_background));
            } else {
                holder.mRootLyt.setBackground(mContext.getResources().getDrawable(R.color.white));
            }
            holder.mRootLyt.getLayoutParams().width = mGridUnit * 32;
        } else {
            holder.mLocationImage.getLayoutParams().height = mGridUnit * 16;
            holder.mLocationImage.getLayoutParams().width = mGridUnit * 16;
            holder.mRootLyt.getLayoutParams().width = mGridUnit * 16;
        }
        setLocationItem(holder, position, item, imageUrl);
    }

    private void setLocationItem(ViewHolder holder, final int position, final Locations item, String imageUrl) {
        if (!TextUtils.isEmpty(item.getTitle()))
            holder.mLocationTitle.setText(item.getTitle());
        else
            holder.mLocationTitle.setText(StringUtility.EMPTY);

        if (!TextUtils.isEmpty(item.getSnippet()))
            holder.mLocationDescription.setText(item.getSnippet());
        else
            holder.mLocationDescription.setText(StringUtility.EMPTY);

        if (!TextUtils.isEmpty(imageUrl)) {
            holder.mLocationImage.setVisibility(View.VISIBLE);
            ImageUtils.setImageThumbnailWithResize(mContext, imageUrl, holder.mLocationImage);
        } else {
            holder.mLocationImage.setImageResource(R.drawable.placeholder_16_9);
        }


        holder.mRootLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLocationItemClickedListener != null)
                    mLocationItemClickedListener.onLocationItemClicked(position, item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLocationDataList == null ? 0 : mLocationDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSwitchView) {
            return LIST_ITEM;
        } else {
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType() {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }


    public interface OnLocationItemClicked {
        void onLocationItemClicked(int position, Locations location);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.location_title)
        TextView mLocationTitle;

        @BindView(R.id.location_description)
        TextView mLocationDescription;

        @BindView(R.id.location_image)
        ImageView mLocationImage;

        @BindView(R.id.lyt_location_item)
        LinearLayout mRootLyt;

        @BindView(R.id.root_item_lyt)
        CardView rootCardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
