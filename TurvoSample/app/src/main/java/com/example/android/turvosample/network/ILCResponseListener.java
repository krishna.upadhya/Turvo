
package com.example.android.turvosample.network;


import android.util.Log;

import rx.Subscriber;

public abstract class ILCResponseListener<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {
        Log.d("RS", "compete");
    }

    @Override
    public void onError(Throwable e) {
        Log.d("RS", "error : " + e.getLocalizedMessage());
        onFailure(e);
    }

    @Override
    public void onNext(T t) {
        Log.d("RS", "on next" + t);
        onSuccess(t);
    }

    public abstract void onSuccess(T response);

    public abstract void onFailure(Throwable error);
}