package com.example.android.turvosample.utils;

/**
 * Created by sapna on 22-04-2017.
 */
public class StringUtility {
    public static final String EMPTY = "";
    public static final String SPACE = " ";
    public static final String NEW_LINE = "\n";
}
