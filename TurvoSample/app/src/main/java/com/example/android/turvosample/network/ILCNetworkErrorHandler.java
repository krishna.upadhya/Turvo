package com.example.android.turvosample.network;


import android.util.Log;

import com.example.android.turvosample.R;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Helper class to manage network errors
 */

public class ILCNetworkErrorHandler {

    public static int getNetworkErrorMessage(Throwable throwable) {
        Log.d("Exception type", throwable.getMessage());
        int errorMsg;
        if (throwable instanceof HttpException) {
            // We had non-2XX http error\
            errorMsg = R.string.http_exception_error;
        } else if (throwable instanceof SocketTimeoutException) {
            //time out exception
            errorMsg = R.string.time_out_error;
        } else if (throwable instanceof IOException) {
            errorMsg = R.string.io_exception_msg;
        } else {
            errorMsg = R.string.http_exception_error;
        }
        return errorMsg;
    }
}
