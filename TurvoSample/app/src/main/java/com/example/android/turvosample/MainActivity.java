package com.example.android.turvosample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.turvosample.Fragment.LocationListFragment;
import com.example.android.turvosample.Fragment.MapMarkerFragment;
import com.example.android.turvosample.utils.ILCFragmentConstants;
import com.example.android.turvosample.utils.ILCFragmentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    private final int HOME_LAYOUT = 0;
    private final int MAP_LAYOUT = 1;

    private static final String TAG_LOCATION_FRAGMENT = "location_fragment";
    private static final String TAG_MAP_FRAGMENT = "map_fragment";

    @BindView(R.id.home_icon)
    ImageView mHomeIcon;

    @BindView(R.id.map_icon)
    ImageView mSettingsIcon;

    @BindView(R.id.home_container)
    FrameLayout mFrameLayout;

    @BindView(R.id.home_txt)
    TextView mHomeText;

    @BindView(R.id.map_text)
    TextView mSettingsText;
    private MenuItem mMenuGridItem;
    private MenuItem mMenuListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setHomeScreenLayout(HOME_LAYOUT);
    }

    private void setHomeScreenLayout(int layoutId) {
        Fragment fragment = null;
        switch (layoutId) {
            case HOME_LAYOUT:
                setLayoutIcon(HOME_LAYOUT);
                fragment = LocationListFragment.newInstance();
                setLayoutFragment(fragment, ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.LOCATION_LIST_FRAGMENT));
                break;
            case MAP_LAYOUT:
                setLayoutIcon(MAP_LAYOUT);
                fragment = MapMarkerFragment.newInstance();
                setLayoutFragment(fragment, ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.MAP_MARKER_FRAGMENT));
                break;
        }
    }

    private void removeAllFragment() {
        Fragment homeFragment = getSupportFragmentManager().findFragmentByTag(ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.LOCATION_LIST_FRAGMENT));
        if (homeFragment != null)
            getSupportFragmentManager().beginTransaction().remove(homeFragment).commit();
        Fragment mapsFragment = getSupportFragmentManager().findFragmentByTag(ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.MAP_MARKER_FRAGMENT));
        if (mapsFragment != null)
            getSupportFragmentManager().beginTransaction().remove(mapsFragment).commit();

    }

    private void setLayoutIcon(int layoutId) {
        switch (layoutId) {
            case HOME_LAYOUT:
                mHomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_48));
                mSettingsIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_marker_gray_48));
                mHomeText.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
                mSettingsText.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
                break;
            case MAP_LAYOUT:
                mHomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_gray_48));
                mSettingsIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_marker_48));
                mHomeText.setTextColor(ContextCompat.getColor(this, R.color.light_gray));
                mSettingsText.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
                break;
        }
    }

    private void setLayoutFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_container, fragment, tag);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        mMenuListItem = menu.findItem(R.id.action_list_icon);
        mMenuGridItem = menu.findItem(R.id.action_grid_icon);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment locationFragment = null;
        switch (id) {
            case R.id.action_grid_icon:
                locationFragment = getSupportFragmentManager().findFragmentByTag(ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.LOCATION_LIST_FRAGMENT));
                if (locationFragment != null) {
                    LocationListFragment fragment = (LocationListFragment) locationFragment;
                    fragment.setGridView();
                    if (mMenuGridItem != null)
                        mMenuGridItem.setVisible(false);
                    if (mMenuListItem != null)
                        mMenuListItem.setVisible(true);
                }
                break;
            case R.id.action_list_icon:
                locationFragment = getSupportFragmentManager().findFragmentByTag(ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.LOCATION_LIST_FRAGMENT));
                if (locationFragment != null) {
                    LocationListFragment fragment = (LocationListFragment) locationFragment;
                    fragment.setListView();
                    if (mMenuGridItem != null)
                        mMenuGridItem.setVisible(true);
                    if (mMenuListItem != null)
                        mMenuListItem.setVisible(false);
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.icon_map_lyt)
    public void onSettingsIconClick() {
        setHomeScreenLayout(MAP_LAYOUT);
    }

    @OnClick(R.id.icon_home_lyt)
    public void onHomeIconClick() {
        setHomeScreenLayout(HOME_LAYOUT);
    }
}
