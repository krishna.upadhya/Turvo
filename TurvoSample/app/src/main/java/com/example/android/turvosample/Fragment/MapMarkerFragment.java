package com.example.android.turvosample.Fragment;


import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.turvosample.R;
import com.example.android.turvosample.entity.LocationData;
import com.example.android.turvosample.entity.Locations;
import com.example.android.turvosample.utils.Constants;
import com.example.android.turvosample.utils.Utility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapMarkerFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap map;
    ArrayList<Locations> markersArray = new ArrayList<>();
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private SupportMapFragment Mapfragment;

    public MapMarkerFragment() {
        // Required empty public constructor
    }

    public static MapMarkerFragment newInstance() {
        // Required empty public constructor
        return new MapMarkerFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map_marker, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadMaps();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(Constants.INTENT_EXTRA_LOCATION_DATA)) {
            Locations location = (Locations) getArguments().getSerializable(Constants.INTENT_EXTRA_LOCATION_DATA);
            if (markersArray != null)
                markersArray = new ArrayList<>();
            markersArray.add(location);
        } else {
            LocationData data = Utility.getLocationFromAssets(getActivity().getApplicationContext());
            if (data != null)
                markersArray = data.getLocationsList();
        }
    }

    private void loadMaps() {
        if (getActivity() == null || isDetached()) return;
        if (markersArray != null && markersArray.size() > 0) {
            FragmentManager fm = getChildFragmentManager();
            Mapfragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (Mapfragment == null) {
                Mapfragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, Mapfragment).commit();
            }
            Mapfragment.getMapAsync(this);
        } else {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (checkLocationPermission())
            setUpMap();
    }

    private void setUpMap() {
        for (int i = 0; i < markersArray.size(); i++) {
            Locations location = markersArray.get(i);
            if (location != null) {
                createMarker(location);
            } else {
                continue;
            }
        }
        if (getArguments() != null && getArguments().containsKey(Constants.INTENT_EXTRA_LOCATION_DATA))
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(markersArray.get(0).getLatitude(), markersArray.get(0).getLongitude()),14f));
        else
            map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(markersArray.get(0).getLatitude(), markersArray.get(0).getLongitude())));
    }

    protected Marker createMarker(Locations location) {
        return map.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .anchor(0.5f, 0.5f)
                .title(location.getTitle())
                .snippet(location.getSnippet()));

    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        setUpMap();

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}
