package com.example.android.turvosample.utils;

import android.support.v4.app.Fragment;

import com.example.android.turvosample.Fragment.LocationListFragment;
import com.example.android.turvosample.Fragment.MapMarkerFragment;

public class ILCFragmentUtils {
    public static String getFragmentTag(int type) {
        switch (type) {
            case ILCFragmentConstants.LOCATION_LIST_FRAGMENT:
                return LocationListFragment.class.getName();

            case ILCFragmentConstants.MAP_MARKER_FRAGMENT:
                return MapMarkerFragment.class.getName();

            default:
                return Fragment.class.getName();
        }
    }
}
