package com.example.android.turvosample.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.turvosample.R;
import com.example.android.turvosample.activity.LocationDetailActivity;
import com.example.android.turvosample.adapters.LocationListAdapter;
import com.example.android.turvosample.entity.LocationData;
import com.example.android.turvosample.entity.Locations;
import com.example.android.turvosample.utils.Constants;
import com.example.android.turvosample.utils.Utility;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationListFragment extends Fragment implements LocationListAdapter.OnLocationItemClicked {

    @BindView(R.id.location_list)
    RecyclerView mLocationRecycler;

    private LocationListAdapter mLocationListAdapter;

    ArrayList<Locations> markersArray = new ArrayList<>();

    public LocationListFragment() {
        // Required empty public constructor
    }

    public static LocationListFragment newInstance() {
        return new LocationListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }


    private void loadData() {
        if (getActivity() == null || isDetached()) return;
        LocationData data = Utility.getLocationFromAssets(getActivity().getApplicationContext());
        if (data != null && data.getLocationsList() != null &&
                data.getLocationsList().size() > 0) {
            markersArray = data.getLocationsList();
            mLocationListAdapter = new LocationListAdapter(getActivity(), markersArray, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            mLocationRecycler.setLayoutManager(layoutManager);
            mLocationRecycler.setAdapter(mLocationListAdapter);
        }
    }

    @Override
    public void onLocationItemClicked(int position, Locations location) {
        Intent intent = new Intent(getActivity(), LocationDetailActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LOCATION_DATA, location);
        startActivity(intent);
    }

    public void setListView() {
        mLocationListAdapter.toggleItemViewType();
        mLocationRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mLocationListAdapter.notifyDataSetChanged();
    }

    public void setGridView() {
        mLocationListAdapter.toggleItemViewType();
        mLocationRecycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mLocationListAdapter.notifyDataSetChanged();
    }
}
