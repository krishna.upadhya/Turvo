package com.example.android.turvosample.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;

import com.example.android.turvosample.Fragment.MapMarkerFragment;
import com.example.android.turvosample.R;
import com.example.android.turvosample.entity.Locations;
import com.example.android.turvosample.utils.Constants;
import com.example.android.turvosample.utils.ILCFragmentConstants;
import com.example.android.turvosample.utils.ILCFragmentUtils;
import com.example.android.turvosample.utils.StringUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationDetailActivity extends AppCompatActivity {

    private Locations mLocation;

    @BindView(R.id.location_title)
    TextView mLocationTitle;

    @BindView(R.id.location_description)
    TextView mLocationDescription;

    @BindView(R.id.toolbar)
    Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_detail);
        ButterKnife.bind(this);
        setSupportActionBar(mToolBar);
        getIntentData();
        initMapFragment();
        initHighlights();

    }

    private void initHighlights() {
        if (mLocation == null) return;
        if (!TextUtils.isEmpty(mLocation.getTitle()))
            mLocationTitle.setText(mLocation.getTitle());
        else
            mLocationTitle.setText(StringUtility.EMPTY);

        if (!TextUtils.isEmpty(mLocation.getSnippet()))
            mLocationDescription.setText(mLocation.getSnippet());
        else
            mLocationDescription.setText(StringUtility.EMPTY);

    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.INTENT_EXTRA_LOCATION_DATA)) {
            mLocation = (Locations) getIntent().getSerializableExtra(Constants.INTENT_EXTRA_LOCATION_DATA);
        }
    }

    private void initMapFragment() {
        Bundle args = new Bundle();
        args.putSerializable(Constants.INTENT_EXTRA_LOCATION_DATA, mLocation);
        MapMarkerFragment webPageFragment = new MapMarkerFragment();
        webPageFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.location_container, webPageFragment, ILCFragmentUtils.getFragmentTag(ILCFragmentConstants.MAP_MARKER_FRAGMENT)).commit();
    }

}
