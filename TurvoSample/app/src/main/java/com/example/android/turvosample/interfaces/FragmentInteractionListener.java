package com.example.android.turvosample.interfaces;

import android.os.Bundle;

public interface FragmentInteractionListener {
    void setFragment(Bundle bundle, int fragmentType, int transType);
}
