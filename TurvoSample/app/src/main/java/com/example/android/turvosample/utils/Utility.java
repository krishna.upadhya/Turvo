package com.example.android.turvosample.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

import com.example.android.turvosample.entity.LocationData;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


/**
 * Created by Krishna Upadhya on 6/16/2017.
 */

public class Utility {

    public static int getScreenWidth(Context context) {
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return metrics.widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
            return 800;
        }
    }

    public static int getScreenGridUnitBy32(Context context) {
        return getScreenWidth((Activity) context) / 32;
    }


    public static int getScreenHeight(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }
    public static LocationData getLocationFromAssets(Context context) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(Constants.LOCATION_ASSETS);
            Reader reader = new InputStreamReader(inputStream);
            Gson gson = new Gson();
            LocationData locationData = gson.fromJson(reader, LocationData.class);
            return locationData;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                    inputStream = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
