package com.example.android.turvosample.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.android.turvosample.interfaces.ILIFragmentInteractionListener;
import com.example.android.turvosample.utils.ILCFragmentUtils;

public class ILABaseActivity extends AppCompatActivity implements ILIFragmentInteractionListener {
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        //((ILCApplication) getApplicationContext()).getRSAppComponent().inject(this);
    }

    /**
     * sets the fragment based on type
     *
     * @param bundle
     * @param fragmentType
     * @param transType
     *
     * o
     */


    @Override
    public void setCurrentFragment(Bundle bundle, int fragmentType, int transType, int frameId) {
        addFragment(bundle, fragmentType, transType, frameId);
    }

    @Override
    public void popTopFragment() {

    }

    @Override
    public void popAllFromStack() {

    }

    @Override
    public android.support.v4.app.Fragment getFragmentByType(int fragmentType) {
        return null;
    }

    @Override
    public String getActiveFragmentTag() {
        return null;
    }

    private void addFragment(Bundle bundle, int fragmentType, int transType, int frameId) {

        if (!isFinishing()) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            //ft.setCustomAnimations(R.anim.anim_slide_left, R.anim.anim_slide_right, R.anim.anim_slide_left, R.anim.anim_slide_right);
            Fragment fragment = null;

            if (fragment == null) {
                fragment = Fragment.instantiate(this,
                        ILCFragmentUtils.getFragmentTag(fragmentType), bundle);
                if (transType == FRAG_ADD) {
                    ft.add(frameId, fragment, ILCFragmentUtils.getFragmentTag(fragmentType));
                } else if (transType == FRAG_REPLACE) {
                    ft.replace(frameId, fragment, ILCFragmentUtils.getFragmentTag(fragmentType));
                } else if (transType == FRAG_REPLACE_WITH_STACK) {
                    ft.replace(frameId, fragment, ILCFragmentUtils.getFragmentTag(fragmentType));
                    ft.addToBackStack(ILCFragmentUtils.getFragmentTag(fragmentType));
                } else if (transType == FRAG_ADD_WITH_STACK) {
                    ft.add(frameId, fragment, ILCFragmentUtils.getFragmentTag(fragmentType));
                    ft.addToBackStack(ILCFragmentUtils.getFragmentTag(fragmentType));
                }
            } else {
                ft.attach(fragment);
            }
            ft.commitAllowingStateLoss();
            fm.executePendingTransactions();
        }
    }
}
