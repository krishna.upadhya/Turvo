package com.example.android.turvosample.utils;

/**
 * Class to handle all fragments constants
 */
public class ILCFragmentConstants {
    public static final int MAP_MARKER_FRAGMENT = 1;
    public static final int LOCATION_LIST_FRAGMENT = 2;
}
